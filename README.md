A fork of Heidi theme to be used on [yakopov.me](https://yakopov.me)

Added more responsiveness for mobile screen plus social buttons, date icons, tags description etc.

Original description:

Heidi
=====

Heidi is a port of Mark Otto's Hyde theme (of Jekyll fame) to Ghost.

Initially hosted at [GitHub](https://github.com/yurri/heidi) to which I have lost second factor access later.
